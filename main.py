# CT60A0201 Ohjelmoinnin perusteet 2017 ohjelmien otsikkotiedot.
# Tekijä: Jesse Peltola
# Opiskelijanumero:  0523140
# Päivämäärä: 12.11.2017
# Yhteistyö ja lähteet, nimi ja yhteistyön muoto:  
# HUOM! KAIKKI KURSSIN TEHTÄVÄT OVAT HENKILÖKOHTAISIA!
######################################################################

#Tuodaan verolib.py
import verolib

#Luodaan auto luokka
class Auto:
    vuosi = ""
    CO2 = 0
    vero = ""

#Luodaan luokka hiilidioksidipäästöistä(0 - 400 g/km ja yli 400 g/km)
class paastoluokat():
    c50 = 0
    c100 = 0
    c150 = 0
    c200 = 0
    c250 = 0
    c300 = 0
    c350 = 0
    c400 = 0
    c1500 = 0
    
#Tehdään valikko
def valikko():
    print ("Anna haluamasi toiminnon numero seuraavasta valikosta:")
    print ("1) Lue ajoneuvotiedot")
    print ("2) Laske ja tulosta verot")
    print ("3) Kirjoita CSV-tiedosto")
    print ("4) Tulosta CSV-tiedoston data näytölle")
    print ("0) Lopeta")

    valinta = int(input("Valintasi: "))
    return valinta

#Luodaan aliohjelma, joka lukee tiedostosta ajoneuvotiedot
def lueTiedot(lista):
    
    #Kysytään käyttäjältä tiedoston nimeä
    tiedosto = input("Anna luettavan tiedoston nimi: ")
    tdsto = open(tiedosto, "r")
    
    while True:
        auto = Auto()
        rivi = tdsto.readline()
        if (rivi == ""):
            break
        tietueet = rivi.split(";")
        if (tietueet[33] == "Co2"):
            continue
        auto.vuosi = (tietueet[1][0:4])
        auto.CO2 = int(tietueet[33])
        lista.append(auto)
    tdsto.close()    
    print("Tiedosto", "'" + tiedosto + "'", "luettu.")
    
    
    return lista
    
#Luodaan luokka, joka laskee verot ja tulostaa
def laskeVerot(lista):
    
    #Luodaan summa muuttujat jokaiselle vuodelle:
    summa2010 = 0
    summa2011 = 0
    summa2012 = 0
    summa2013 = 0
    summa2014 = 0
    summa2015 = 0
    summa2016 = 0
    
    #Luodaan jokaiselle vuodelle omat päästöluokat:
    vuosi2010 = paastoluokat()
    vuosi2011 = paastoluokat()
    vuosi2012 = paastoluokat()
    vuosi2013 = paastoluokat()
    vuosi2014 = paastoluokat()
    vuosi2015 = paastoluokat()
    vuosi2016 = paastoluokat()
    
    for i in lista:
        if(i.vuosi == "2010"):
            summa2010 += verolib.vero(i.CO2)
            ##Tehdään myös hiilidioksidiluokittelu samalla
            if i.CO2 < 50:
                vuosi2010.c50 += verolib.vero(i.CO2)
            elif 50 <= i.CO2 < 100:
                vuosi2010.c100 += verolib.vero(i.CO2)
            elif 100 <= i.CO2 < 150:
                vuosi2010.c150 += verolib.vero(i.CO2)
            elif 150 <= i.CO2 < 200:
                vuosi2010.c200 += verolib.vero(i.CO2)
            elif 200 <= i.CO2 < 250:
                vuosi2010.c250 += verolib.vero(i.CO2)
            elif 250 <= i.CO2 < 300:
                vuosi2010.c300 += verolib.vero(i.CO2)
            elif 300 <= i.CO2 < 350:
                vuosi2010.c350 += verolib.vero(i.CO2)
            elif 350 <= i.CO2 < 400:
                vuosi2010.c400 += verolib.vero(i.CO2)
            elif 400 <= i.CO2:
                vuosi2010.c1500 += verolib.vero(i.CO2)
                
        elif(i.vuosi == "2011"):
            summa2011 += verolib.vero(i.CO2)
            if i.CO2 < 50:
                vuosi2011.c50 += verolib.vero(i.CO2)
            elif 50 <= i.CO2 < 100:
                vuosi2011.c100 += verolib.vero(i.CO2)
            elif 100 <= i.CO2 < 150:
                vuosi2011.c150 += verolib.vero(i.CO2)
            elif 150 <= i.CO2 < 200:
                vuosi2011.c200 += verolib.vero(i.CO2)
            elif 200 <= i.CO2 < 250:
                vuosi2011.c250 += verolib.vero(i.CO2)
            elif 250 <= i.CO2 < 300:
                vuosi2011.c300 += verolib.vero(i.CO2)
            elif 300 <= i.CO2 < 350:
                vuosi2011.c350 += verolib.vero(i.CO2)
            elif 350 <= i.CO2 < 400:
                vuosi2011.c400 += verolib.vero(i.CO2)
            elif 400 <= i.CO2:
                vuosi2011.c1500 += verolib.vero(i.CO2)
                
        elif(i.vuosi == "2012"):
            summa2012 += verolib.vero(i.CO2)
            if i.CO2 < 50:
                vuosi2012.c50 += verolib.vero(i.CO2)
            elif 50 <= i.CO2 < 100:
                vuosi2012.c100 += verolib.vero(i.CO2)
            elif 100 <= i.CO2 < 150:
                vuosi2012.c150 += verolib.vero(i.CO2)
            elif 150 <= i.CO2 < 200:
                vuosi2012.c200 += verolib.vero(i.CO2)
            elif 200 <= i.CO2 < 250:
                vuosi2012.c250 += verolib.vero(i.CO2)
            elif 250 <= i.CO2 < 300:
                vuosi2012.c300 += verolib.vero(i.CO2)
            elif 300 <= i.CO2 < 350:
                vuosi2012.c350 += verolib.vero(i.CO2)
            elif 350 <= i.CO2 < 400:
                vuosi2012.c400 += verolib.vero(i.CO2)
            elif 400 <= i.CO2:
                vuosi2012.c1500 += verolib.vero(i.CO2)

        elif(i.vuosi == "2013"):
            summa2013 += verolib.vero(i.CO2)
            if i.CO2 < 50:
                vuosi2013.c50 += verolib.vero(i.CO2)
            elif 50 <= i.CO2 < 100:
                vuosi2013.c100 += verolib.vero(i.CO2)
            elif 100 <= i.CO2 < 150:
                vuosi2013.c150 += verolib.vero(i.CO2)
            elif 150 <= i.CO2 < 200:
                vuosi2013.c200 += verolib.vero(i.CO2)
            elif 200 <= i.CO2 < 250:
                vuosi2013.c250 += verolib.vero(i.CO2)
            elif 250 <= i.CO2 < 300:
                vuosi2013.c300 += verolib.vero(i.CO2)
            elif 300 <= i.CO2 < 350:
                vuosi2013.c350 += verolib.vero(i.CO2)
            elif 350 <= i.CO2 < 400:
                vuosi2013.c400 += verolib.vero(i.CO2)
            elif 400 <= i.CO2:
                vuosi2013.c1500 += verolib.vero(i.CO2)

        elif(i.vuosi == "2014"):
            summa2014 += verolib.vero(i.CO2)
            if i.CO2 < 50:
                vuosi2014.c50 += verolib.vero(i.CO2)
            elif 50 <= i.CO2 < 100:
                vuosi2014.c100 += verolib.vero(i.CO2)
            elif 100 <= i.CO2 < 150:
                vuosi2014.c150 += verolib.vero(i.CO2)
            elif 150 <= i.CO2 < 200:
                vuosi2014.c200 += verolib.vero(i.CO2)
            elif 200 <= i.CO2 < 250:
                vuosi2014.c250 += verolib.vero(i.CO2)
            elif 250 <= i.CO2 < 300:
                vuosi2014.c300 += verolib.vero(i.CO2)
            elif 300 <= i.CO2 < 350:
                vuosi2014.c350 += verolib.vero(i.CO2)
            elif 350 <= i.CO2 < 400:
                vuosi2014.c400 += verolib.vero(i.CO2)
            elif 400 <= i.CO2:
                vuosi2014.c1500 += verolib.vero(i.CO2)

            
        elif(i.vuosi == "2015"):
            summa2015 += verolib.vero(i.CO2)
            if i.CO2 < 50:
                vuosi2015.c50 += verolib.vero(i.CO2)
            elif 50 <= i.CO2 < 100:
                vuosi2015.c100 += verolib.vero(i.CO2)
            elif 100 <= i.CO2 < 150:
                vuosi2015.c150 += verolib.vero(i.CO2)
            elif 150 <= i.CO2 < 200:
                vuosi2015.c200 += verolib.vero(i.CO2)
            elif 200 <= i.CO2 < 250:
                vuosi2015.c250 += verolib.vero(i.CO2)
            elif 250 <= i.CO2 < 300:
                vuosi2015.c300 += verolib.vero(i.CO2)
            elif 300 <= i.CO2 < 350:
                vuosi2015.c350 += verolib.vero(i.CO2)
            elif 350 <= i.CO2 < 400:
                vuosi2015.c400 += verolib.vero(i.CO2)
            elif 400 <= i.CO2:
                vuosi2015.c1500 += verolib.vero(i.CO2)

        elif(i.vuosi == "2016"):
            summa2016 += verolib.vero(i.CO2)
            if i.CO2 < 50:
                vuosi2016.c50 += verolib.vero(i.CO2)
            elif 50 <= i.CO2 < 100:
                vuosi2016.c100 += verolib.vero(i.CO2)
            elif 100 <= i.CO2 < 150:
                vuosi2016.c150 += verolib.vero(i.CO2)
            elif 150 <= i.CO2 < 200:
                vuosi2016.c200 += verolib.vero(i.CO2)
            elif 200 <= i.CO2 < 250:
                vuosi2016.c250 += verolib.vero(i.CO2)
            elif 250 <= i.CO2 < 300:
                vuosi2016.c300 += verolib.vero(i.CO2)
            elif 300 <= i.CO2 < 350:
                vuosi2016.c350 += verolib.vero(i.CO2)
            elif 350 <= i.CO2 < 400:
                vuosi2016.c400 += verolib.vero(i.CO2)
            elif 400 <= i.CO2:
                vuosi2016.c1500 += verolib.vero(i.CO2)
        
    print("Verokertymät vuosittain 2010-luvulla ovat seuraavat:")
    print("2010", round(summa2010), "euroa.")
    print("2011", round(summa2011), "euroa.")
    print("2012", round(summa2012), "euroa.")
    print("2013", round(summa2013), "euroa.")
    print("2014", round(summa2014), "euroa.")
    print("2015", round(summa2015), "euroa.")
    print("2016", round(summa2016), "euroa.")

   ##Luodaan jokaiselle vuodelle omat listat, johon tulee dataa:
    paastoluokka = [";" + str(50) + ";" + str(100) + ";" + str(150) + ";" + str(200) + ";" + str(250) + ";" + str(300) + ";" + str(350) + ";" \
                    + str(400) + ";" + str(1000) + ";"]  
    data2010 = ["2010" + ";" + str(round(vuosi2010.c50)) + ";" + str(round(vuosi2010.c100)) + ";" + str(round(vuosi2010.c150)) \
                + ";" + str(round(vuosi2010.c200)) + ";" +  str(round(vuosi2010.c250)) + ";" +  str(round(vuosi2010.c300))\
                + ";" +  str(round(vuosi2010.c350)) + ";" + str(round(vuosi2010.c400)) + ";" + str(round(vuosi2010.c1500)) + ";"]
    data2011 = ["2011" + ";" + str(round(vuosi2011.c50)) + ";" + str(round(vuosi2011.c100)) + ";" + str(round(vuosi2011.c150)) \
                + ";" + str(round(vuosi2011.c200)) + ";" +  str(round(vuosi2011.c250)) + ";" +  str(round(vuosi2011.c300))\
                + ";" +  str(round(vuosi2011.c350)) + ";" + str(round(vuosi2011.c400)) + ";" + str(round(vuosi2011.c1500)) + ";"]
    data2012 = ["2012" + ";" + str(round(vuosi2012.c50)) + ";" + str(round(vuosi2012.c100)) + ";" + str(round(vuosi2012.c150)) \
                + ";" + str(round(vuosi2012.c200)) + ";" +  str(round(vuosi2012.c250)) + ";" +  str(round(vuosi2012.c300))\
                + ";" +  str(round(vuosi2012.c350)) + ";" + str(round(vuosi2012.c400)) + ";" + str(round(vuosi2012.c1500)) + ";"]
    data2013 = ["2013" + ";" + str(round(vuosi2013.c50)) + ";" + str(round(vuosi2013.c100)) + ";" + str(round(vuosi2013.c150)) \
                + ";" + str(round(vuosi2013.c200)) + ";" +  str(round(vuosi2013.c250)) + ";" +  str(round(vuosi2013.c300))\
                + ";" +  str(round(vuosi2013.c350)) + ";" + str(round(vuosi2013.c400)) + ";" + str(round(vuosi2013.c1500)) + ";"]
    data2014 = ["2014" + ";" + str(round(vuosi2014.c50)) + ";" + str(round(vuosi2014.c100)) + ";" + str(round(vuosi2014.c150)) \
                + ";" + str(round(vuosi2014.c200)) + ";" +  str(round(vuosi2014.c250)) + ";" +  str(round(vuosi2014.c300))\
                + ";" +  str(round(vuosi2014.c350)) + ";" + str(round(vuosi2014.c400)) + ";" + str(round(vuosi2014.c1500)) + ";"]
    data2015 = ["2015" + ";" + str(round(vuosi2015.c50)) + ";" + str(round(vuosi2015.c100)) + ";" + str(round(vuosi2015.c150)) \
                + ";" + str(round(vuosi2015.c200)) + ";" +  str(round(vuosi2015.c250)) + ";" +  str(round(vuosi2015.c300))\
                + ";" +  str(round(vuosi2015.c350)) + ";" + str(round(vuosi2015.c400)) + ";" + str(round(vuosi2015.c1500)) + ";"]
    data2016 = ["2016" + ";" + str(round(vuosi2016.c50)) + ";" + str(round(vuosi2016.c100)) + ";" + str(round(vuosi2016.c150)) \
                + ";" + str(round(vuosi2016.c200)) + ";" +  str(round(vuosi2016.c250)) + ";" +  str(round(vuosi2016.c300))\
                + ";" +  str(round(vuosi2016.c350)) + ";" + str(round(vuosi2016.c400)) + ";" + str(round(vuosi2016.c1500)) + ";"]

    #Lisätään datat yhteen listaan, jolloin listan vienti toiseen aliohjelmaan on helpompaa
    kaikkidata = [paastoluokka,
                  data2010,
                  data2011,
                  data2012,
                  data2013,
                  data2014,
                  data2015,
                  data2016]
    return kaikkidata

###Luodaan aliohjelma, joka kirjoittaa datat CSV-tiedostoon
def kirjoitaTiedostoon(kaikkidata):
    
    #Pyydetään käyttäjältä tiedoston nimen
    tiedosto = input("Anna kirjoitettavan tiedoston nimi: ")
    tdsto = open(tiedosto, "w")

    for i in kaikkidata: 
        listarivi = "" 
        for a in i:
            listarivi = listarivi + str(a) + "\n"
            tdsto.write(listarivi)
            
    print("CSV-tiedosto kirjoitettu.")
    return tiedosto

##Luodaan aliohjelma, joka näyttää datat näytölle
def datat(tiedosto):
    
    lue = open(tiedosto, "r")
    print("CSV-tiedoston data on seuraava:")
    for i in lue:
        print(i, end = "")

##Pääohjelma
def main():
    lista = []
    while True:
        valinta = valikko()
        if (valinta == 1):
            lue = lueTiedot(lista)
        elif (valinta == 2):
            vero = laskeVerot(lue)
        elif (valinta == 3):
            kirjoita = kirjoitaTiedostoon(vero) #lopussa kirjoita = tiedosto
        elif (valinta == 4):
            tulosta = datat(kirjoita)
        elif (valinta == 0):
            break
    print("Kiitos ohjelman käytöstä.")
            
        
#Kutsutaan pääohjelmaa
main()

######################################################################
# eof
